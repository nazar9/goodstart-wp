<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package PROJECT_NAME
 */

get_header(); ?>

	<div id="primary" class="l-wrap">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="title text-center"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'project_name' ); ?></h1>
					<p class="text-center"><a class="c-btn" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Back to homepage', 'project_name' ); ?></a></p>
				</header><!-- .page-header -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
