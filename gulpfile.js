// Imports
const {src, dest, watch, series, parallel} = require('gulp');
const touch        = require('gulp-touch-fd');
const sass         = require('gulp-sass');
const sassGlob     = require('gulp-sass-glob');
const postcss      = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssnano      = require("cssnano");
const gulpif       = require('gulp-if');
const sourcemaps   = require('gulp-sourcemaps');
const include      = require("gulp-include");
const uglify       = require('gulp-uglify-es').default;
const eslint       = require('gulp-eslint');
const cache        = require('gulp-cache');
const imagemin     = require('gulp-imagemin');
const browserSync  = require('browser-sync').create();
const fs           = require('fs');
const htmlInjector = require("bs-html-injector");
const tailwindcss  = require('tailwindcss');
const prefixWrap   = require("postcss-prefixwrap");
const babel        = require('gulp-babel');

// Setup enviroment variables
var envType          = process.env.NODE_ENV;
try {
  var options = require('./options.js');
  // Local enviroment setup override
  var isStatic       = options.isStatic;
  var isWP           = options.isWP;
  var templateNameWP = options.templateNameWP;
} catch(err) {
  // Default enviroment setup (in most cases for deployment)
  var isStatic       = false;  // Static website
  var isWP           = true;   // WordPress Project
  var templateNameWP = 'theme-dir-name';
}

// Local server
function server(done) {
  // Run server
  if(isStatic) {
    browserSync.use(htmlInjector, { files: "./static/*.html" });
    browserSync.init({ server: { baseDir: "./static" }});
  }
  else {
    browserSync.use(htmlInjector, { files: "wordpress/**/*.php" });
    browserSync.init({ proxy: options.serverLink });
  }
	done(); // Signal completion
}

// Reload the browser when files change
function reloadBrowser(done) {
  console.log("\n\t Reloading Browser Preview.\n");
	browserSync.reload();
	done();
}

// Compile styles
function styles() {
  return src('src-assets/sass/main.sass')
    .pipe(gulpif(envType == 'development', sourcemaps.init({ loadMaps: true })))
    .pipe(sassGlob())
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(dest('src-assets/sass'))
    .pipe(gulpif(envType == 'development', sourcemaps.init({ loadMaps: true })))
    .pipe(
      postcss([
        tailwindcss(),
        require('postcss-viewport-height-correction'),
      ])
    )
    .pipe(gulpif(envType == 'production',
      postcss([
        autoprefixer(),
        cssnano()
      ]),
    ))
    .pipe(gulpif(envType == 'development', sourcemaps.write() ))
    .pipe(gulpif(isStatic,dest('./static/assets/css')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(touch())
    .pipe(browserSync.stream({ match: '**/*.css' }));
}

// Import styles from external packages
function import_styles() {
  return src('src-assets/sass/import.sass')
    .pipe(gulpif(envType == 'development', sourcemaps.init({ loadMaps: true })))
    .pipe(sassGlob())
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(
      postcss([
        tailwindcss(),
        require('postcss-viewport-height-correction'),
      ])
    )
    .pipe(gulpif(envType == 'production',
      postcss([
        autoprefixer(),
        cssnano()
      ]),
    ))
    .pipe(gulpif(envType == 'development', sourcemaps.write() ))
    .pipe(gulpif(isStatic,dest('./static/assets/css')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(touch());
}

function stylesAdminACFBlocks() {
  return src(['src-assets/sass/admin-acf-blocks.sass'])
    .pipe(gulpif(envType == 'development', sourcemaps.init({ loadMaps: true })))
    .pipe(sassGlob())
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(dest('src-assets/sass'))
    .pipe(
      postcss([
        tailwindcss(),
        require('postcss-viewport-height-correction'),
        prefixWrap(".acf-block-preview"),
      ])
    )
    .pipe(gulpif(envType == 'production',
      postcss([
        autoprefixer(),
        cssnano()
      ]),
    ))
    .pipe(gulpif(envType == 'development', sourcemaps.write('.') ))
    .pipe(gulpif(isStatic,dest('./static/assets/css')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(touch())
    .pipe(browserSync.stream({ match: '**/*.css' }));
}

function stylesAdminGlobal() {
  return src(['src-assets/sass/admin-global.sass', 'src-assets/sass/editor-styles.sass'])
    .pipe(sassGlob())
    .pipe(sass()).on("error", sass.logError)
    .pipe(postcss([
      autoprefixer(),
      cssnano()
    ]))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(touch());
}

// Scripts tasks
function scriptsVendor() {
  return src('src-assets/js/vendor/*.js')
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(include())
        .on('error', console.log)
      .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulpif(isStatic,dest('./static/assets/js/vendor')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/js/vendor')))
    .pipe(touch());
}

// Check main JS file syntax & minify it
function scriptMain() {
  return src('src-assets/js/main.js')
    .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(include())
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
    .pipe(sourcemaps.write('../js'))
    .pipe(gulpif(isStatic,dest('./static/assets/js')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/js')))
    .pipe(touch());
}

// Copy fonts to appropriate folder
function fonts() {
  return src(['src-assets/fonts/**/*.*', 'node_modules/@fortawesome/fontawesome-free/webfonts/**/*.*'])
    .pipe(gulpif(isStatic,dest('./static/assets/fonts')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/fonts')));
}

// Optimize all images
function images() {
  return src('src-assets/img/**/*')
  .pipe(cache(imagemin([
    // svg
    imagemin.svgo({
        plugins: [
          { removeViewBox: false },
          { cleanupIDs: true }
        ]
    }),
    ])))
    .pipe(gulpif(isStatic,dest('./static/assets/img')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/img')));
}

// Watch for changes
function watchSource() {
  watch(['src-assets/sass/**/*.(sass|scss)'], series(styles));
  watch(['src-assets/sass/import.(sass|scss)'], series(import_styles, reloadBrowser));
  watch(['wordpress/themes/theme-dir-name/**/*.php'], series(styles));
  watch('tailwind.config.js', series(styles));
  watch('src-assets/sass/admin-acf-blocks.sass', series(stylesAdminACFBlocks));
  watch(['src-assets/sass/admin-global.sass', 'src-assets/sass/editor-styles.sass'], series(stylesAdminGlobal));
  watch('./static/*.html', series(htmlInjector));
  watch('src-assets/js/vendor/*.js', series(scriptsVendor, reloadBrowser));
  watch('src-assets/js/main.js', series(scriptMain, reloadBrowser));
  watch('src-assets/fonts/**/*.*', series(fonts, reloadBrowser));
  watch('src-assets/img/**/*.+(png|jpg|jpeg|gif|svg)', series(images));
  console.log("\n\t Watching for Changes..\n");
};

// Basic tasks
exports.basic = series(
  parallel(
    styles,
    import_styles,
    stylesAdminACFBlocks,
    stylesAdminGlobal,
    scriptsVendor,
    scriptMain,
    fonts,
    images
  )
);

// Tasks for compiling project on CI/CD
exports.build = series(
  parallel(
    styles,
    import_styles,
    stylesAdminACFBlocks,
    stylesAdminGlobal,
    scriptsVendor,
    scriptMain,
    fonts,
    images
  )
);
// Alternative name for the deployment task group
exports.deploy = series(
  exports.build
);

// Basic + Browser Sync + Watch
// Development tasks
exports.default = series(
  exports.basic,
  server,
  watchSource
);
