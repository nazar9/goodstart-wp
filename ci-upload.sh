#!/bin/bash

# Upload current directory to remote server
# Usage in gitlab-ci.yml:
# - cd a/b/c
# - bash $CI_PROJECT_DIR/ci-upload.sh $CI_PATH/sub/di/rec/to/ry/-/on/-/server/

# CI_PROTOCOL values:
# FTP: Unencrypted FTP
# FTPS: Encrypted FTP (certificate errors silenced)
# RSYNC: SSH using rsync
# SFTP: SSH using sftp protocol (with the program lftp)


# Fail on errors
set -e

# Check if all the binaries are installed
if [ -n "$CI_DEBUG" ]; then
  lftp --version
  rsync --version
  ssh -V
fi

REMOTE_DEST_PATH="$1"
SOURCE_PATH=`pwd`
RUN_ENVIRONMENT="$2"

if [ "z$RUN_ENVIRONMENT" == "zSTAGING" ]; then
        echo "Using staging environment"
        CI_HOST="$CI_HOST_STAGING"
        CI_USERNAME="$CI_USERNAME_STAGING"
        CI_PASSWORD="$CI_PASSWORD_STAGING"
        CI_PATH="$CI_PATH_STAGING"
        CI_THEMENAME="$CI_THEMENAME_STAGING"
        CI_PROTOCOL="$CI_PROTOCOL_STAGING"
fi

# Check if all the variables are set
if [ -z "$CI_HOST" ]; then echo 'CI_HOST not set'; exit 1; fi
if [ -z "$CI_USERNAME" ]; then echo 'CI_USERNAME not set'; exit 1; fi
if [ -z "$CI_PASSWORD" ]; then echo 'CI_PASSWORD not set'; exit 1; fi
if [ -z "$CI_PATH" ]; then echo 'CI_PATH not set'; exit 1; fi
if [ -z "$CI_THEMENAME" ]; then echo 'CI_THEMENAME not set'; exit 1; fi
if [ -z "$CI_PROTOCOL" ]; then echo 'CI_PROTOCOL not set'; exit 1; fi
if [ -z "$REMOTE_DEST_PATH" ]; then echo 'ci-upload.sh called without a parameter'; exit 1; fi

if [ -z "$CI_SSH_PORT" ]; then
  echo "CI_SSH_PORT not set, defaulting to 22"
  CI_SSH_PORT=22
fi

if [ -n "$CI_DEBUG" ]; then
  echo "Uploading to $CI_HOST dir $REMOTE_DEST_PATH from $SOURCE_PATH via $CI_PROTOCOL"
fi

if [ "$CI_PROTOCOL" == "FTP" ]; then
  echo "machine $CI_HOST" >>$HOME/.netrc
  echo "  login $CI_USERNAME" >>$HOME/.netrc
  echo "  password $CI_PASSWORD" >>$HOME/.netrc
  lftp -c "set ftp:ssl-allow no; set ssl:verify-certificate no; open -u $CI_USERNAME $CI_HOST; mirror --delete -Rev ./ $REMOTE_DEST_PATH --ignore-time --transfer-all --parallel=10 --exclude-glob .git* --exclude .git/ --exclude node_modules/ --exclude bower_components/ --exclude .npm/"
elif [ "$CI_PROTOCOL" == "FTPS" ]; then
  echo "machine $CI_HOST" >>$HOME/.netrc
  echo "  login $CI_USERNAME" >>$HOME/.netrc
  echo "  password $CI_PASSWORD" >>$HOME/.netrc
  echo lftp -c "set ftp:ssl-allow yes; set ssl:verify-certificate no; open -u $CI_USERNAME $CI_HOST; mirror --delete -Rev ./ $REMOTE_DEST_PATH --ignore-time --transfer-all --parallel=10 --exclude-glob .git* --exclude .git/ --exclude node_modules/ --exclude bower_components/ --exclude .npm/"
  lftp -c "set ftp:ssl-allow yes; set ssl:verify-certificate no; open -u $CI_USERNAME $CI_HOST; mirror --delete -Rev ./ $REMOTE_DEST_PATH --ignore-time --transfer-all --parallel=10 --exclude-glob .git* --exclude .git/ --exclude node_modules/ --exclude bower_components/ --exclude .npm/"
  rm $HOME/.netrc
elif [ "$CI_PROTOCOL" == "RSYNC" ]; then
  mkdir -p ~/.ssh
  chmod 0700 ~/.ssh
  ssh-keyscan -p $CI_SSH_PORT -H $CI_HOST >>~/.ssh/known_hosts
  ssh-keyscan -p $CI_SSH_PORT -t ecdsa -H $CI_HOST >>~/.ssh/known_hosts
  chmod 0600 ~/.ssh/known_hosts
  echo $CI_PASSWORD >~/.rsyncpass
  chmod 0400 ~/.rsyncpass
  rsync --rsh="/usr/bin/sshpass -f$HOME/.rsyncpass ssh -o StrictHostKeyChecking=no -l \"$CI_USERNAME\"" -avz --delete --exclude '.git*' --exclude '.git/'  --exclude 'node_modules/' --exclude 'bower_components/'  --exclude '.npm/' ./ "$CI_HOST:$REMOTE_DEST_PATH/"
  rm ~/.rsyncpass
elif [ "$CI_PROTOCOL" == "SFTP" ]; then
  mkdir -p ~/.ssh
  chmod 0700 ~/.ssh
  ssh-keyscan -p $CI_SSH_PORT -H $CI_HOST >>~/.ssh/known_hosts
  ssh-keyscan -p $CI_SSH_PORT -t ecdsa -H $CI_HOST >>~/.ssh/known_hosts
  chmod 0600 ~/.ssh/known_hosts
  echo "machine $CI_HOST" >>$HOME/.netrc
  echo "  login $CI_USERNAME" >>$HOME/.netrc
  echo "  password $CI_PASSWORD" >>$HOME/.netrc
  lftp -c "open -u $CI_USERNAME sftp://$CI_HOST; mirror --delete -Rev ./ $REMOTE_DEST_PATH --ignore-time --transfer-all --parallel=10 --exclude-glob .git* --exclude .git/ --exclude node_modules/ --exclude bower_components/ --exclude .npm/"
else
  echo "Unsupported protocol $CI_PROTOCOL - use one of: FTP FTPS SFTP RSYNC"
  exit 1
fi