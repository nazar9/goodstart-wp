/*******************************************************************************

  Project: PROJECT_NAME
  Author:  PROJECT_AUTHOR
  Date:    PROJECT_DATE

********************************************************************************

TOC:
  Scroll to top
  Off canvas menu
  Header
  Dropdown Menu
  Lightbox for images and videos
  Responsive Videos
  Scoll Animations Init
  Object Fit Images
  Contact Form 7 Fix
  Accordion
  Map
  Equal Heights
  Footer fix
  Fix group widths
  AJAX Load More
  Block - Image Slider
  Fix 100vh
  Parallax

*******************************************************************************/

jQuery( document ).ready(function($) {

  // Useful global variables
  var $adminBar = $('#wpadminbar'); 
  var adminBarEnabled = $adminBar.length ? true : false;

/* Scroll to top
------------------------------------------------------------------------------*/

  $(document).on( 'scroll', function(){
    if ($(window).scrollTop() > 200) {
      $('.c-scroll-to-top').addClass('show');
    } else {
      $('.c-scroll-to-top').removeClass('show');
    }
  });

  $('.c-scroll-to-top').on('click', function(event) {
    event.preventDefault();
    var verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
    var element = $('body');
    var offset = element.offset();
    var offsetTop = offset.top;
    $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
  });

/* Off canvas panel
------------------------------------------------------------------------------*/

  var targetElement = document.querySelector('.js-main-menu-mobile-scroll');
  var $header = $('.js-header');
  var $navWrap = $('.js-nav-wrap');
  var $panelTrigger = $('.js-menu-toggle');
  var $panelCover = $('.js-cover');
  var $panelClose = $('.js-panel-close');
  var $panel = $('.js-nav');

  function showPanel() {
    setMenuTopPadding();
    bodyScrollLock.disableBodyScroll(targetElement, { reserveScrollBarGap: false});
    $panelTrigger.addClass('is-active');
    $panelCover.removeClass('pointer-events-none opacity-0');
    $panel.removeClass('-translate-y-full xs:translate-y-0 xs:translate-x-full');
    $panel.addClass('shadow-xl');
  }

  function hidePanel() {
    $panelTrigger.removeClass('is-active');
    $panelCover.addClass('pointer-events-none opacity-0');
    bodyScrollLock.clearAllBodyScrollLocks();
    $panel.addClass('-translate-y-full xs:translate-y-0 xs:translate-x-full');
    $panel.removeClass('shadow-xl');
  }

  function setMenuTopPadding() {
    if(adminBarEnabled) {
      $navWrap.css('padding-top', $header.outerHeight() + $adminBar.outerHeight() + 'px');
    }
    else {
      $navWrap.css('padding-top', $header.outerHeight() + 'px');
    }
  }

  setMenuTopPadding();

  $panelTrigger.on('click', function(){
    if($panelCover.hasClass('pointer-events-none')) {
      showPanel();
    }
    else {
      hidePanel();
    }
  });

  $panelClose.on('click', function(){
    hidePanel();
  });

  $panelCover.on('click', function(){
    hidePanel();
  });

  // Hide navigation when user pressed Escape key on keyboard
  $(document).keyup(function(e) {
    if (e.keyCode === 27) {
      if ($panelTrigger.hasClass('is-active')) {
        hidePanel();
      }
    }
  });

  // Hide mobile navigation when user increases browser size
  var mediaQueryTablet = window.matchMedia('(min-width: 768px)');

  function hideNavOnLargeScreens(e) {
    // Check if the media query is true
    if (e.matches) {
      // Then log the following message to the console
      hidePanel();
    }
  }
  // Register event listener
  mediaQueryTablet.addListener(hideNavOnLargeScreens);
  // Initial check
  hideNavOnLargeScreens(mediaQueryTablet);

  $('ul.c-mobile-menu li').has('ul').addClass('parentLi');
  $('ul.c-mobile-menu li ul').addClass('sub-menu');

  $('ul.c-mobile-menu .sub-menu').hide();

  $('ul.c-mobile-menu .parentLi > a').on('click', function(event){
    event.preventDefault();
    $(this).toggleClass('expanded');
    $(this).parent().find('ul.sub-menu').slideToggle();
  });

/* Header 
------------------------------------------------------------------------------*/

  // Function which controls header status according to the distance from top
  function setHeaderStatus(scroll){
    if (scroll >= 1) { $('.js-header').addClass('js-header--scrolled'); }
    else {
      if(!($('body').hasClass('hc-nav-open'))) {
        $('.js-header').removeClass('js-header--scrolled');
      }
    }
  }

  // Set status on load
  setHeaderStatus($(window).scrollTop());

  // Set status during the scroll
  $(window).scroll(function() {
    setHeaderStatus($(window).scrollTop());
  });

  // https://codingreflections.com/hide-header-on-scroll-down/
  var doc = document.documentElement;
  var w = window;

  var prevScroll = w.scrollY || doc.scrollTop;
  var curScroll;
  var direction = 0;
  var prevDirection = 0;

  var header = document.querySelector('.js-header');

  var checkScroll = function() {

    /*
    ** Find the direction of scroll
    ** 0 - initial, 1 - up, 2 - down
    */

    curScroll = w.scrollY || doc.scrollTop;
    if (curScroll > prevScroll) {
      //scrolled up
      direction = 2;
    }
    else if (curScroll < prevScroll) {
      //scrolled down
      direction = 1;
    }

    if (direction !== prevDirection) {
      toggleHeader(direction, curScroll);
    }

    prevScroll = curScroll;
  };

  var toggleHeader = function(direction, curScroll) {
    if (direction === 2 && curScroll > 52) {

      //replace 52 with the height of your header in px

      header.classList.add('-translate-y-full');
      prevDirection = direction;
    }
    else if (direction === 1) {
      header.classList.remove('-translate-y-full');
      prevDirection = direction;
    }
  };

  window.addEventListener('scroll', checkScroll);

/* Dropdown Menu
------------------------------------------------------------------------------*/

  if($('.c-hor-menu').length) {
    $('.c-hor-menu .menu-item-has-children > a').on('click', function(e){
      e.preventDefault();
      $(this).parent().toggleClass('opened');
    });
  }

  // Hide dropdown after click outside the menu
  $(document).on('click', function(event) {
    if (!$(event.target).closest('.c-hor-menu .menu-item-has-children a').length) {
      $('.c-hor-menu .menu-item-has-children').removeClass('opened');
    }
  });

/* Lightbox for images and videos
------------------------------------------------------------------------------*/

  $('a[href$=jpg], a[href$=jpeg], a[href$=jpe], a[href$=png], a[href$=gif]').magnificPopup({
    type: 'image',
    removalDelay: 500,
    mainClass: 'mfp-with-fade',
    closeOnContentClick: true,
    midClick: true
  });

  $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-with-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
    iframe: {
			patterns: {
        youtube: {
          index: 'youtube.com/',
          id: function(url) {
                  var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
                  if ( !m || !m[1] ) return null;
                  return m[1];
              },
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        },
				youtube_short: {
				  index: 'youtu.be/',
				  id: 'youtu.be/',
				  src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
				}
			}
		}
  });

/* Responsive Videos
/*******************************************************************************/

  fitvids({
    // players: 'iframe[src*="example.com"]'
  });

/* Scoll Animations Init
/*******************************************************************************/

  AOS.init({
    disable: 'mobile',
    once: true
  });

  window.addEventListener('load', AOS.refresh);

  function refreshAOS() {
    AOS.refresh();
  }

  $('img').on('load', _.throttle(refreshAOS, 1000));

/* 9. Contact Form 7 Fix
/*******************************************************************************/

  $('.wpcf7-form p:empty').remove();

/* Accordion
/*******************************************************************************/

  // Hide all accordions
  $('.c-accordion__text:not(.c-accordion__text--active)').hide();
  var allPanels = $('.c-accordion__text');
  var allTitles = $('.c-accordion__subtitle');

  $('.c-accordion__subtitle > a').click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var $title = $this.parent();
    var $target =  $this.parent().next();

    if(!$target.hasClass('c-accordion__text--active')){
      allPanels.removeClass('c-accordion__text--active').slideUp();
      allTitles.removeClass('c-accordion__subtitle--active');
      $target.addClass('c-accordion__text--active').slideDown();
      $title.addClass('c-accordion__subtitle--active');
    }
    else {
      allPanels.removeClass('c-accordion__text--active').slideUp();
      allTitles.removeClass('c-accordion__subtitle--active');
    }
    return false;
  });

/* Map
/*******************************************************************************/

  $.each($('.dyn-map'), function(){

    var lat = $(this).data('lat');
    var long = $(this).data('long');
    var zoom = $(this).data('zoom');

    L.Icon.Default.imagePath = theme_settings.template_dir + '/assets/img/leaflet/';
    var mymap = new L.map($(this)[0], {
    scrollWheelZoom: false
    }).setView([lat, long], 16);

    // create custom icon
    var pinIcon = L.icon({
        iconUrl: theme_settings.template_dir + '/assets/img/leaflet/pin.svg',
        shadowUrl:  theme_settings.template_dir + '/assets/img/leaflet/marker-shadow.png',
        iconSize: [40, 50], // size of the icon
        iconAnchor:   [16, 50],
        shadowAnchor: [10, 39],
        popupAnchor:  [5, -48]
    });

    // Open Street Map
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        maxZoom: zoom
    }).addTo(mymap);

    if($(this).data('popup')) {
      L.marker([lat, long], {icon: pinIcon}).addTo(mymap)
      .bindPopup($(this).data('popup'))
      .openPopup();
    }

    // Open Street Map - Wikimedia
    // L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {
    //     attribution: '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
    //     maxZoom: zoom
    // }).addTo(mymap);

    // Open Street Map - Black and White
    /*
    L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        maxZoom: zoom
    }).addTo(mymap);
    */

    // Mapbox - remember to change accessToken
    /*
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: zoom,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiYWRhbXBpbGNoIiwiYSI6ImNqbTY0aGlzeTB0eTEza280dGVpcjVnemMifQ.zZjr5GSWWpe4lYGBjvuz1A'
    }).addTo(mymap);
    */

    mymap.on('click', function() {
    if (mymap.scrollWheelZoom.enabled()) {
      mymap.scrollWheelZoom.disable();
      }
      else {
      mymap.scrollWheelZoom.enable();
      }
    });

  });

/* Equal Heights
------------------------------------------------------------------------------*/

  // Example:
  // $('.something').matchHeight();

  // Match Height compatibility with Lazy Load techniques
  function refreshMatchHeight() {
    $.fn.matchHeight._update();
  }
  $('img').on('load', _.throttle(refreshMatchHeight, 1000));

/* Footer fix
------------------------------------------------------------------------------*/

  function fixFooter() {
    if($('body').outerHeight() < $(window).outerHeight()) {
      var footerHeight = $('.js-footer').outerHeight();
      var bodyHeight = $('body').outerHeight();
      var windowHeight = $(window).outerHeight();
      var contentHeight = bodyHeight - footerHeight;
      $('.js-footer').css('min-height', windowHeight - contentHeight + 'px');
    }
  }

  fixFooter();
  $(window).on("debouncedresize", function( event ) { fixFooter(); });

/* Fix group widths
------------------------------------------------------------------------------*/

  $('.wp-block-group').each(function( index ) {
    if($(this).hasClass('alignfull')) {
      $(this).wrap( '<div class="wp-block-group-wrap alignfull"></div>' );
    }
    else if($(this).hasClass('alignwide')) {
      $(this).wrap( '<div class="wp-block-group-wrap alignwide"></div>' );
    }
    else {
      $(this).wrap( '<div class="wp-block-group-wrap"></div>' );
    }
  });

/* AJAX Load More
------------------------------------------------------------------------------*/

  $('.js-load-more').click(function(){
    var button = $(this);
    var content_place = $(this).parent();
    var data = {
      'action': 'ajax_load_more',
      'query': theme_settings.posts,
      'page' : theme_settings.current_page
    };

    if (!(button.hasClass('c-btn--post-loading-loader') || button.hasClass('c-btn--post-no-more-posts'))) {

      $.ajax({
        url : theme_settings.ajaxurl,
        data : data,
        type : 'POST',
        beforeSend : function ( xhr ) {
          button.addClass('c-btn--post-loading-loader').find('span').html(theme_settings.loading);
        },
        success : function( data ){
          var tempScrollTop = $(window).scrollTop();
          if( data.length ) {
            button.removeClass('c-btn--post-loading-loader').find('span').html(theme_settings.loadmore);
            $(data).insertBefore(content_place);
            theme_settings.current_page++;

            if ( theme_settings.current_page == theme_settings.max_page )
              button.removeClass('c-btn--post-loading-loader').addClass('c-btn--post-no-more-posts').find('span').html(theme_settings.noposts);
          } else {
            button.removeClass('c-btn--post-loading-loader').addClass('c-btn--post-no-more-posts').find('span').html(theme_settings.noposts);
          }
          $(window).scrollTop(tempScrollTop);
        }
      });
    }
  });

/* Block - Image Slider
------------------------------------------------------------------------------*/

  document.querySelectorAll('.js-image-slider').forEach(function(elem) {
    new Swiper(elem, {
      loop: false,
      speed: 700,
      autoHeight: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      },
      navigation: {
        nextEl: elem.nextElementSibling.nextElementSibling,
        prevEl: elem.nextElementSibling,
      },
      on: {
        init: function () {
          var sliderWraper = $(this.$el).parent();
          var prevArrowPrev = sliderWraper.find('.swiper-button-prev');
          var prevArrowNext = sliderWraper.find('.swiper-button-next');
          var imageHeight = sliderWraper.find('img').first().height();

          prevArrowPrev.css('top', imageHeight / 2 + 'px');
          prevArrowNext.css('top', imageHeight / 2 + 'px');
        },
        slideChange: function() {
          var sliderWraper = $(this.$el).parent();
          var prevArrowPrev = sliderWraper.find('.swiper-button-prev');
          var prevArrowNext = sliderWraper.find('.swiper-button-next');
          var imageHeight = $(sliderWraper.find('img')[this.activeIndex]).height();

          prevArrowPrev.css('top', imageHeight / 2 + 'px');
          prevArrowNext.css('top', imageHeight / 2 + 'px');
        }
      },
    });
  });

/* Fix 100vh
------------------------------------------------------------------------------*/
  
var customViewportCorrectionVariable = 'vh';

function setViewportProperty(doc) {
  var prevClientHeight;
  var customVar = '--' + ( customViewportCorrectionVariable || 'vh' );
  function handleResize() {
    var clientHeight = doc.clientHeight;
    if (clientHeight === prevClientHeight) return;
    requestAnimationFrame(function updateViewportHeight(){
      doc.style.setProperty(customVar, (clientHeight * 0.01) + 'px');
      prevClientHeight = clientHeight;
    });
  }
  handleResize();
  return handleResize;
}
window.addEventListener('resize', setViewportProperty(document.documentElement));

}); // end of jQuery code